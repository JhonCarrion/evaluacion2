from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name = 'estudiantes'),
    path('nuevo', views.crear),
    path('editar', views.modificar),
    path('baja', views.baja),
    path('eliminar', views.eliminar),
]
