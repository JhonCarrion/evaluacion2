from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from .forms import FormularioEstudiante
from app.modelo.models import Estudiante
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def home(request):
    usuario = request.user
    print(usuario.get_all_permissions())
    if usuario.has_perm('modelo.view_estudiante'):
        listaEstudiantes = Estudiante.objects.all().order_by('apellidos')
        context = {
            'estudiantes': listaEstudiantes,
            'title': "Estudiantes",
            'mensaje': "Modulo Estudiantes"
        }
        return render(request, 'estudiante/home.html', context)
    else:
        messages.warning(request, 'No Permitido')
        return render(request, 'login/403.html')

@login_required
def crear(request):
    usuario = request.user
    if usuario.has_perm('modelo.add_estudiante'):
        formulario = FormularioEstudiante(request.POST)
        if request.method == 'POST':
            if formulario.is_valid():
                datos = formulario.cleaned_data
                estudiante = Estudiante()
                estudiante.cedula = datos.get('cedula')
                estudiante.matricula = datos.get('matricula')
                estudiante.carrera = datos.get('carrera')
                estudiante.nombres = datos.get('nombres')
                estudiante.apellidos = datos.get('apellidos')
                estudiante.genero = datos.get('genero')
                estudiante.estadoCivil = datos.get('estadoCivil')
                estudiante.fechaNacimiento = datos.get('fechaNacimiento')
                estudiante.correo = datos.get('correo')
                estudiante.telefono = datos.get('telefono')
                estudiante.celular = datos.get('celular')
                estudiante.direccion = datos.get('direccion')
                if datos.get('estado') == "on" :
                    estudiante.estado = True
                else:
                    estudiante.estado = False
                estudiante.save()
                messages.warning(request, 'Guardado Exitosamente')
                return redirect(home)
        context = {
            'f': formulario,
            'title': "Ingresar Estudiante",
            'mensaje': "Ingresar Nuevo Estudiante"
        }
        return render(request, 'estudiante/crear.html', context)
    else:
        messages.warning(request, 'No Permitido')
        return render(request, 'login/403.html')

@login_required
def modificar(request):
    usuario = request.user
    if usuario.has_perm('modelo.change_estudiante'):
        if request.GET['cedula']:
            dni = request.GET['cedula'];
            estudiante = Estudiante.objects.get(cedula=dni)
        else:
            if request.GET['matricula']:
                dni = request.GET['matricula'];
                estudiante = Estudiante.objects.get(matricula=dni)

        formulario = FormularioEstudiante(instance=estudiante)

        if request.method == 'POST':
            estudiante.cedula = request.POST['cedula']
            estudiante.matricula = request.POST['matricula']
            estudiante.carrera = request.POST['carrera']
            estudiante.apellidos = request.POST['apellidos']
            estudiante.nombres = request.POST['nombres']
            estudiante.genero = request.POST['genero']
            estudiante.estadoCivil = request.POST['estadoCivil']
            estudiante.fechaNacimiento = request.POST['fechaNacimiento']
            estudiante.correo = request.POST['correo']
            estudiante.telefono = request.POST['telefono']
            estudiante.celular = request.POST['celular']
            estudiante.direccion = request.POST['direccion']
            if 'estado' in request.POST:
                if request.POST['estado'] == "on":
                    estudiante.estado = True
                else:
                    estudiante.estado = False
            else:
                estudiante.estado = False
            estudiante.save()
            messages.warning(request, 'Datos Modificados')
            return redirect(home)
        context = {
            'f': formulario,
            'title': "Modificar Estudiantee",
            'mensaje': "Modificar datos de " + estudiante.nombres + " " + estudiante.apellidos
        }
        return render(request, 'estudiante/crear.html', context)
    else:
        messages.warning(request, 'No Permitido')
        return render(request, 'login/403.html')


@login_required
def baja(request):
    usuario = request.user
    if usuario.has_perm('modelo.delete_estudiante'):
        if request.GET['cedula']:
            dni = request.GET['cedula'];
            estudiante = Estudiante.objects.get(cedula=dni)
        else:
            if request.GET['matricula']:
                dni = request.GET['matricula'];
                estudiante = Estudiante.objects.get(matricula=dni)

        if estudiante:
            if estudiante.estado == True:
                estudiante.estado = False
                messages.warning(request, 'Estudiante Desactivado')
            else:
                estudiante.estado = True
                messages.warning(request, 'Estudiante Reactivado')
            estudiante.save()
            return redirect(home)
        else:
            messages.warning(request, 'Perdido')
            return redirect(home)
        return render(request, 'estudiante/crear.html', context)
    else:
        messages.warning(request, 'No Permitido')
        return render(request, 'login/403.html')


@login_required
def eliminar(request):
    usuario = request.user
    if usuario.has_perm('modelo.delete_estudiante'):
        if request.GET['cedula']:
            dni = request.GET['cedula'];
            estudiante = Estudiante.objects.get(cedula=dni)
        else:
            if request.GET['matricula']:
                dni = request.GET['matricula'];
                estudiante = Estudiante.objects.get(matricula=dni)

        if estudiante:
            if estudiante.delete():
                messages.warning(request, 'Estudiante Eliminado')
            else:
                messages.warning(request, 'No se pudo Eliminar')
            return redirect(home)
        else:
            messages.warning(request, 'Perdido')
            return redirect(home)
        return render(request, 'estudiante/crear.html', context)
    else:
        messages.warning(request, 'No Permitido')
        return render(request, 'login/403.html')
