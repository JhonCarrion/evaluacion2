from django.db import models

# Create your models here.
class Estudiante(models.Model):

    listaGenero = (
        ('f', 'Femenino'),
        ('m', 'Masculino'),
    )
    listaEstadoCivil = (
        ('soltero', 'Solter@'),
        ('casado', 'Casad@'),
        ('viudo', 'Viud@'),
        ('divorciado', 'Divorciad@'),
        ('unionLibre', 'Unión Libre'),
    )

    estudiante_id = models.AutoField(primary_key=True)
    cedula = models.CharField(unique=True, max_length=10, null = False)
    matricula = models.PositiveIntegerField(unique=True, null = False)
    carrera = models.CharField(max_length=70, null = False)
    nombres = models.CharField(max_length=70, null = False)
    apellidos = models.CharField(max_length=70, null = False)
    genero = models.CharField(max_length=15, choices = listaGenero, null = False)
    estadoCivil = models.CharField(max_length=15, choices = listaEstadoCivil, null = False)
    fechaNacimiento = models.DateField(auto_now = False, auto_now_add = False, null = False)
    correo = models.EmailField(unique=True, max_length=100, null = False)
    telefono = models.CharField(max_length=15, null = False)
    celular = models.CharField(max_length=15, null = False)
    direccion = models.TextField(null = False)
    estado = models.BooleanField(null = False, default = True)

    def __str__(self):
        return self.cedula
